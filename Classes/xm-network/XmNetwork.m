//
//  XmNetwork.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "XmNetwork.h"
#import "XmNetworkServiceProtocol.h"
#import "XmHTTPService.h"
#import "XmAppDelegate.h"

NSErrorUserInfoKey const XmNSURLSessionDataTaskKey = @"XmNSURLSessionDataTaskKey";
NSErrorUserInfoKey const XmNetworkResponseObjectKey = @"XmNetworkResponseObjectKey";

NSErrorDomain const XmNetworkErrorDomain = @"XmNetworkErrorDomain";


@implementation XmNetworkConfig
+ (instancetype)shared
{
    static XmNetworkConfig *netConfig = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        netConfig = [[XmNetworkConfig alloc] init];
    });
    
    return netConfig;
}

- (void)updateConfig:(NSDictionary *)config
{
    self.ossAccessKeyId = [config valueForKeyPath:@"oss.accessKeyId"];
    self.ossAccessKey = [config valueForKeyPath:@"oss.secretAccessKey"];
    self.ossBucketName = [config valueForKeyPath:@"oss.bucketName"];
    self.ossHost = [config valueForKeyPath:@"oss.cdn"];
    self.name = [config objectForKey:@"name"];
    self.baseUrl = [config objectForKey:@"httpBaseUrl"];
    self.apiVersion = [config objectForKey:@"apiVersion"];
    self.env = [config objectForKey:@"env"];
    self.socket = [config objectForKey:@"socket"];
}

@end

@interface XmNetwork ()

@property (nonatomic, strong) id<XmNetworkInterceptorProtocol> interceptor;
@property (nonatomic, strong) XmNetworkConfig *config;

@property (nonatomic, strong) id<XmHTTPServiceProtocol> httpService;
//@property (nonatomic, strong) id<XmOSSUploadServiceProtocol> uploadService;

@property (nonatomic, assign) BOOL debugMode;

@end

@implementation XmNetwork {
    struct XmNetworkInterceptor {
        unsigned int handleError                : 1;
        unsigned int handleErrorResponseObject  : 1;
        unsigned int requestCustom              : 1;
    } _interceptorFlags;
}


+ (void)load {
  xm_register_module(self);
}

- (instancetype)initWithConfig:(NSDictionary *)config {
  self = [super init];
  if (self) {
      _config = [XmNetworkConfig shared];
      _config.ossAccessKeyId = [config valueForKeyPath:@"oss.accessKeyId"];
      _config.ossAccessKey = [config valueForKeyPath:@"oss.secretAccessKey"];
      _config.ossBucketName = [config valueForKeyPath:@"oss.bucketName"];
      _config.ossHost = [config valueForKeyPath:@"oss.cdn"];
      _config.name = [config objectForKey:@"name"];
      _config.baseUrl = [config objectForKey:@"httpBaseUrl"];
      _config.apiVersion = [config objectForKey:@"apiVersion"];
      
      [self _setup];
  }
  return self;
}

- (void)_setup {
    _httpService = [[XmHTTPService alloc] init];
//    _uploadService = [[XmOssUploadService alloc] initWithAccessKeyId:_config.ossAccessKeyId secretAccessKey:_config.ossAccessKey bucketName:_config.ossBucketName directoryName:_config.name ossHost:_config.ossHost];
}

- (void)setInterceptor:(id<XmNetworkInterceptorProtocol>)interceptor {
    _interceptor = interceptor;
    
    _interceptorFlags.handleError = [interceptor respondsToSelector:@selector(handleErrorResponse:)];
    _interceptorFlags.handleErrorResponseObject = [interceptor respondsToSelector:@selector(handleErrorResponse:responseObject:)];
    _interceptorFlags.requestCustom = [interceptor respondsToSelector:@selector(requestWithIncompleteRequest:)];
}

// MARK: - Protocol

// MARK: - Debug Mode
+ (void)startDebugMode {
#ifdef DEBUG
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    module.debugMode = YES;
#endif
}

+ (void)stopDebugMode {
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    module.debugMode = NO;
}

// MARK: - 注册网络请求前、后处理对象
+ (void)registerNetworkInterceptor:(id<XmNetworkInterceptorProtocol>)interceptor {
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    module.interceptor = interceptor;
}

// MARK: - HTTP Service
+ (RACSignal<id> *)GET:(NSString *)URLString parameters:(id)parameters {
    return [self request:GET URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)GET:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    return [self request:GET service:service URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)POST:(NSString *)URLString parameters:(id)parameters {
    return [self request:POST URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)POST:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    return [self request:POST service:service URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)DELETE:(NSString *)URLString parameters:(id)parameters {
    return [self request:DELETE URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)DELETE:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    return [self request:DELETE service:service URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)PUT:(NSString *)URLString parameters:(id)parameters {
    return [self request:PUT URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)PUT:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    return [self request:PUT service:service URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)request:(XmHTTPMethod)method URLString:(NSString *)URLString parameters:(id)parameters {
    return [self _request:method URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)request:(XmHTTPMethod)method service:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    // 拼接 service/version/url
    URLString = [NSString stringWithFormat:@"/%@/%@%@", service, module.config.apiVersion, URLString];
    return [self _request:method URLString:URLString parameters:parameters];
}

+ (RACSignal<id> *)_request:(XmHTTPMethod)method URLString:(NSString *)URLString parameters:(id)parameters {
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    XmRequest *request = [module requestWithMethod:method URLString:URLString parameters:parameters];
    return [[[module _request:request] flattenMap:^RACSignal *(id value) {
        return [module interceptorsResponse:value request:request];
    }] map:^id (id responseObject) {
        // 只返回data中的数据
        id data = [responseObject objectForKey:@"data"];
        if ([data isEqual:[NSNull null]]) {
            data = nil;
        }
        return data;
    }];
}

// MARK: - Instanse Method

- (RACSignal<id> *)request:(XmHTTPMethod)method URLString:(NSString *)URLString parameters:(id)parameters {
    return [self _request:method URLString:URLString parameters:parameters];
}
- (RACSignal<id> *)request:(XmHTTPMethod)method service:(NSString *)service URLString:(NSString *)URLString parameters:(id)parameters {
    URLString = [NSString stringWithFormat:@"/%@/%@%@", service, self.config.apiVersion, URLString];
    return [self _request:method URLString:URLString parameters:parameters];
}

- (RACSignal<id> *)_request:(XmHTTPMethod)method URLString:(NSString *)URLString parameters:(id)parameters {
    return [self _request:[self requestWithMethod:method URLString:URLString parameters:parameters]];
}

- (RACSignal<id> *)_request:(XmRequest *)request {
    if (_interceptorFlags.requestCustom) {
        request = [[_interceptor requestWithIncompleteRequest:request] copy];
    }
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self logMessage:[NSString stringWithFormat:@"|------------------ BEGIN ------------------|%@\n|------------------- END -------------------|", request]];
        // STEP1: 拿到响应数据
        [[self.httpService request:request] subscribeNext:^(id x) {
            [subscriber sendNext:x];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            // 401
            NSURLSessionDataTask *task = [error.userInfo objectForKey:XmNSURLSessionDataTaskKey];
            NSInteger code = ((NSHTTPURLResponse *)task.response).statusCode;
            if (code == 401) {
                // 拿到httpStatusCode为401下的响应数据中的code值
                [subscriber sendNext:@{@"code" : @(101)}];
                [subscriber sendCompleted];
            } else {
                NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                userInfo[@"msg"] = @"网络开小差啦~";
                userInfo[NSLocalizedDescriptionKey] = @"网络开小差啦~";
                [subscriber sendError:error];
            }
        }];
        return nil;
    }];
}

- (RACSignal<id> *)interceptorsResponse:(id)responseObject request:(XmRequest *)request {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSInteger code = [[responseObject objectForKey:@"success"] integerValue];
            if (code == 1) {
                [subscriber sendNext:responseObject];
                [subscriber sendCompleted];
            } else {
                if (_interceptorFlags.handleError || _interceptorFlags.handleErrorResponseObject) {
                    RACSignal<id> *signal;
                    if (_interceptorFlags.handleError) {
                        signal = [self.interceptor handleErrorResponse:code];
                    } else {
                        signal = [self.interceptor handleErrorResponse:code responseObject:responseObject];
                    }
                    @weakify(self);
                    [signal subscribeNext:^(id x) {
                        @strongify(self);
                        [[self _request:request] subscribeNext:^(id x) {
                            [subscriber sendNext:x];
                            [subscriber sendCompleted];
                        } error:^(NSError *error) {
                            [subscriber sendError:error];
                        }];
                    } error:^(NSError *error) {
                        [subscriber sendError:error];
                    }];
                } else {
                    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:responseObject];
                    userInfo[NSLocalizedDescriptionKey] = @"无法处理响应数据异常，请调用[XmNetwork registerNetworkInterceptor:] 方法注册拦截器";
                    NSError *error = [NSError errorWithDomain:XmNetworkErrorDomain code:XmNetworkErrorUnableHandleErrorResponse userInfo:userInfo];
                    [subscriber sendError:error];
                }
            }
        } else {
            NSError *error = [NSError errorWithDomain:XmNetworkErrorDomain code:XmNetworkErrorResponseDataInvalid userInfo:@{NSLocalizedDescriptionKey : @"接口响应数据格式错误", XmNetworkResponseObjectKey : [responseObject isEqual:[NSNull null]] ? @{} : responseObject }];
            [subscriber sendError:error];
        }
        return nil;
    }];
}

// MARK: - Utils
- (XmRequest *)requestWithMethod:(XmHTTPMethod)method URLString:(NSString *)URLString parameters:(id)parameters {
    if (![URLString hasPrefix:@"http"] && self.config.baseUrl) {
        // 拼接base url
        NSString *baseUrl = [self.config.baseUrl copy];
        if ([baseUrl hasSuffix:@"/"]) {
            // 1. 确保base url不以"/"结束
            baseUrl = [baseUrl substringToIndex:baseUrl.length - 1];
        }
        if (![URLString hasPrefix:@"/"]) {
            // 2. 确保path以"/开始"
            URLString = [NSString stringWithFormat:@"/%@", URLString];
        }
        URLString = [NSString stringWithFormat:@"%@%@", baseUrl, URLString];
    }
    return [[XmRequest alloc] initWithMethod:method Url:URLString parameters:parameters];
}

- (void)logMessage:(NSString *)message{
    if (self.debugMode) {
        fprintf(stderr, "\n%s\n", message.UTF8String);
    }
}

// MARK: - HTTP Download Service
/**
 HTTP 下载
 
 @param sourceURL 远程文件URL eg: https://cdn.yryz.com/yryz-new/image/3C2E31BF-C7E0-4DB8-A225-0420DF37A642.jpg
 @return 下载信息
 */
+ (RACSignal<XmDownloadInfo *> *)downloadWithSourceURL:(NSURL *)sourceURL {
    return [self downloadWithSourceURL:sourceURL targetURL:nil];
}

/**
 HTTP 下载
 
 @param sourceURL 远程文件URL eg: https://cdn.yryz.com/yryz-new/image/3C2E31BF-C7E0-4DB8-A225-0420DF37A642.jpg
 @param targetURL 目标目录URL，未传默认放在沙盒Cache文件下 eg: ../cache/
 @return 下载信息
 */
+ (RACSignal<XmDownloadInfo *> *)downloadWithSourceURL:(NSURL *)sourceURL targetURL:(NSURL *)targetURL {
    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
    return [module.httpService downloadWithSourceURL:sourceURL targetURL:targetURL];
}


// MARK: - OSS Upload Service
/**
 上传图片
 
 @param uploadData 上传图片数据
 @return 上传结果
 */
+ (RACSignal<XmUploadInfo *> *)uploadWithData:(NSData *)uploadData {
    return [self uploadWithData:uploadData fileType:nil];
}

/**
 上传音视频
 
 @param fileURL 文件地址
 @return 上传结果
 */
+ (RACSignal<XmUploadInfo *> *)uploadWithFileURL:(NSURL *)fileURL {
    return [self uploadWithFileURL:fileURL fileType:nil];
}

/**
 上传图片
 
 @param uploadData 上传图片数据
 @param fileType 上传数据类型 head（头像），image（图片），audio（音频），video（视频）
 @return 上传结果
 */
//+ (RACSignal<XmUploadInfo *> *)uploadWithData:(NSData *)uploadData fileType:(NSString *)fileType {
//    if (!uploadData) {
//        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//            NSError *error = [NSError errorWithDomain:XmNetworkErrorDomain code:XmNetworkErrorInvalidUploadData userInfo:@{@"message" : @"无效的uploadData."}];
//            [subscriber sendError:error];
//            return nil;
//        }];
//    }
//    XmNetwork *module = xm_get_module_instance(XmNetwork.class);
//    return [module.uploadService uploadWithData:uploadData fileType:fileType];
//}

/**
 上传文件
 
 @param fileURL 文件地址
 @param fileType 上传数据类型 head（头像），image（图片），audio（音频），video（视频）
 @return 上传结果
 */
//+ (RACSignal<XmUploadInfo *> *)uploadWithFileURL:(NSURL *)fileURL fileType:(NSString *)fileType {
//    if (!fileURL) {
//        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//            NSError *error = [NSError errorWithDomain:XmNetworkErrorDomain code:XmNetworkErrorInvalidFileURL userInfo:@{@"message" : @"无效的fileURL."}];
//            [subscriber sendError:error];
//            return nil;
//        }];
//    }
//    if (!fileURL.isFileURL) {
//        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//            NSError *error = [NSError errorWithDomain:XmNetworkErrorDomain code:XmNetworkErrorInvalidFileURL userInfo:@{@"message" : @"fileURL不是有效的文件URL."}];
//            [subscriber sendError:error];
//            return nil;
//        }];
//
//    }
//    XmNetwork *module = Xm_get_module_instance(XmNetwork.class);
//    return [module.uploadService uploadWithFileURL:fileURL fileType:fileType];
//}



@end
