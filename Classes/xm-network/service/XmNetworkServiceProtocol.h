//
//  XmNetworkServiceProtocal.h
//  Pods
//
//  Created by cysu on 2020/11/18.
//


#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "XmRequest.h"

FOUNDATION_EXPORT NSErrorUserInfoKey const XmNSURLSessionDataTaskKey;
FOUNDATION_EXPORT NSErrorUserInfoKey const XmNetworkResponseObjectKey;
FOUNDATION_EXPORT NSErrorDomain const XmNetworkErrorDomain;
NS_ERROR_ENUM(XmNetworkErrorDomain)
{
    XmNetworkErrorResponseDataInvalid          = -1000,     // 接口响应数据格式错误
    XmNetworkErrorDownloadFailed               = -1001,     // 下载失败
    XmNetworkErrorInvalidSourceURL             = -1002,     // 无效的文件URL
    XmNetworkErrorUnableHandleErrorResponse    = -1003,     // 无法处理响应数据异常，请调用[XmNetwork registerNetworkInterceptor:] 方法注册拦截器
    
    XmNetworkErrorLackOSSParams                = -2001,     // 缺少oss上传参数
    XmNetworkErrorInvalidFileURL               = -2002,     // 无效的文件URL
    XmNetworkErrorInvalidUploadData            = -2003,     // 无效的上传NSData
    XmNetworkErrorUploadFailed                 = -2004,     // 上传失败
};

@class XmDownloadInfo;
@protocol XmHTTPServiceProtocol <NSObject>

- (RACSignal<id> *)request:(XmRequest *)request;

- (RACSignal<XmDownloadInfo *> *)downloadWithSourceURL:(NSURL *)sourceURL targetURL:(NSURL *)targetURL;

@end

@class XmUploadInfo;
@protocol XmOSSUploadServiceProtocol <NSObject>

- (RACSignal<XmUploadInfo *> *)uploadWithData:(NSData *)uploadData fileType:(NSString *)fileType;
- (RACSignal<XmUploadInfo *> *)uploadWithFileURL:(NSURL *)fileURL fileType:(NSString *)fileType;

@end
