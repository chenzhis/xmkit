//
//  XmHTTPService.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>
#import "XmNetworkServiceProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface XmHTTPService : NSObject<XmHTTPServiceProtocol>

@end

NS_ASSUME_NONNULL_END
