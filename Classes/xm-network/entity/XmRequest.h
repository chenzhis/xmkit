//
//  XmRequest.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, XmHTTPMethod) {
    GET,
    POST,
    DELETE,
    PUT,
    PATCH
};


@interface XmRequest : NSObject<NSCopying>

@property (nonatomic, assign) XmHTTPMethod method;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) id parameters;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSString *> *allHTTPHeaderFields;

- (instancetype)initWithMethod:(XmHTTPMethod)method Url:(NSString *)url parameters:(id)parameters;


@end

NS_ASSUME_NONNULL_END
