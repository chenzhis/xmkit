//
//  XmRequest.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "XmRequest.h"

@implementation XmRequest


- (instancetype)initWithMethod:(XmHTTPMethod)method Url:(NSString *)url parameters:(id)parameters {
    if (self = [super init]) {
        _method = method;
        _url = [url copy];
        _parameters = [parameters copy];
        _allHTTPHeaderFields = [NSMutableDictionary dictionary];
    }
    return self;
}

// MARK: - NSCopying
- (id)copyWithZone:(NSZone *)zone {
    XmRequest *request = [[XmRequest alloc] initWithMethod:_method Url:_url parameters:_parameters];
    request.allHTTPHeaderFields = [_allHTTPHeaderFields mutableCopy];
    return request;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nurl: %@\nmethod: %@\nheader: %@\nparams: %@", _url, [self httpMethod:_method], _allHTTPHeaderFields, _parameters];
}

- (NSString *)httpMethod:(XmHTTPMethod)method {
    switch (method) {
        case GET:
            return @"GET";
            break;
        case POST:
            return @"POST";
            break;
        case DELETE:
            return @"DELETE";
            break;
        case PUT:
            return @"PUT";
            break;
        case PATCH:
            return @"PATCH";
            break;
    }
}

@end
