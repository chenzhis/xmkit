//
//  XmDownloadInfo.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XmDownloadInfo : NSObject


@property (nonatomic, assign) int64_t totalBytesReceive;                // 已下载字节数
@property (nonatomic, assign) int64_t totalBytesExpectedToReceive;      // 总下载字节数

@property (nonatomic, assign, getter=isCompleted) BOOL completed;
@property (readonly, nonatomic, assign) float progress; // 0.0 ~ 1.0

@property (nonatomic, copy) NSString *url;


@end

NS_ASSUME_NONNULL_END
