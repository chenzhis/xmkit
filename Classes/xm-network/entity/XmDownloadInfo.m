//
//  XmDownloadInfo.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "XmDownloadInfo.h"

@implementation XmDownloadInfo


- (float)progress {
    return (float)_totalBytesReceive / _totalBytesExpectedToReceive;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Progress: %.2f, URL: %@", self.progress, _url];
}

@end
