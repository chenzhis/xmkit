//
//  XmAppDelegate.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "XmAppDelegate.h"
#include <objc/runtime.h>


NSString *const XmCancel = @"0";

static NSMutableArray<Class> *xmModuleClasses;
NSMutableArray<id<XmModule>> *xmModules;
static dispatch_queue_t xmModuleClassesSyncQueue;

void xm_register_module(Class moduleClass)
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        xmModuleClasses = [NSMutableArray new];
        xmModules= [NSMutableArray new];
        xmModuleClassesSyncQueue = dispatch_queue_create("xm.react.ModuleClassesSyncQueue", DISPATCH_QUEUE_CONCURRENT);
    });
    
    // Register module
    dispatch_barrier_async(xmModuleClassesSyncQueue, ^{
        [xmModuleClasses addObject:moduleClass];
    });
}

id xm_get_module_instance(Class moduleClass){
    for (id<XmModule> module in xmModules) {
        if([module isKindOfClass:moduleClass])
            return module;
    }
    return nil;
}


@implementation XmAppDelegate

- (BOOL)application:(UIApplication*)application
willFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    return YES;
}


- (BOOL)application:(UIApplication*)application
didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    
    
//    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"env" withExtension:@"json"];
//    /// 有根据接口切换环境
//    if ([YdkNetworkConfig shared].env) {
//        NSString *path = [NSString stringWithFormat:@"env_%@", [YdkNetworkConfig shared].env];
//        fileURL = [[NSBundle mainBundle] URLForResource:path withExtension:@"json"];
//    }
//    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
//    _config = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    for(Class cls in xmModuleClasses){
        id<XmModule> module = nil;
        Method initMethod = class_getInstanceMethod(cls, @selector(initWithConfig:));
        if(initMethod!=nil){
            module = [[cls alloc] initWithConfig:_config];
        }else{
            module = [[cls alloc] init];
        }
        [xmModules addObject:module];
        
    }
    
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            if (![module application:application didFinishLaunchingWithOptions:launchOptions]) {
                return NO;
            }
        }
        
    }
    return YES;
}


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
- (void)application:(UIApplication*)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings*)notificationSettings {
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            [module application:application didRegisterUserNotificationSettings:notificationSettings];
        }
    }
    
}
#pragma GCC diagnostic pop

- (void)application:(UIApplication*)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            [module application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
        }
    }
    
}

- (void)application:(UIApplication*)application
didReceiveRemoteNotification:(NSDictionary*)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            [module application:application  didReceiveRemoteNotification:userInfo
         fetchCompletionHandler:completionHandler];
        }
    }
    
}

- (void)application:(UIApplication*)application
didReceiveLocalNotification:(UILocalNotification*)notification {
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            [module application:application didReceiveLocalNotification:notification];
        }
    }
    
}


- (BOOL)application:(UIApplication*)application
            openURL:(NSURL*)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey, id>*)options {
    for (id<XmModule> module in xmModules) {
        if ([module respondsToSelector:_cmd]) {
            if (! [module  application:application openURL:url options:options]){
                return NO;
            }
        }
    }
    return YES;
    
}


#pragma mark -session delegate
-(void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    
    NSURLSessionAuthChallengeDisposition disposition = NSURLSessionAuthChallengePerformDefaultHandling;
    __block NSURLCredential *credential = nil;
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        if (credential) {
            disposition = NSURLSessionAuthChallengeUseCredential;
        } else {
            disposition = NSURLSessionAuthChallengePerformDefaultHandling;
        }
    } else {
        disposition = NSURLSessionAuthChallengePerformDefaultHandling;
    }
    
    if (completionHandler) {
        completionHandler(disposition, credential);
    }
}




@end
