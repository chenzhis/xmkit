//
//  XmConfig.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>
#import "XmModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface XmConfig : NSObject<XmModule>

+ (instancetype _Nonnull )shared;
- (nullable id)valueForKeyPath:(NSString *_Nonnull)keyPath;

@property (nonatomic, copy) NSString * _Nonnull httpBaseUrl;
@property (nonatomic, copy) NSString * _Nonnull webBaseUrl;

@end

NS_ASSUME_NONNULL_END
