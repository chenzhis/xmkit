//
//  XmAppDelegate.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <UIKit/UIKit.h>
#import "XmConstants.h"
#import "XmModule.h"

NS_ASSUME_NONNULL_BEGIN

void xm_register_module(Class moduleClass);
id xm_get_module_instance(Class moduleClass);

@interface XmAppDelegate : UIResponder<UIApplicationDelegate,NSURLSessionDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (readonly, nonatomic, copy) NSDictionary *config;

@end

NS_ASSUME_NONNULL_END
