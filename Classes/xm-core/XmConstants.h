//
//  XmConstants.h
//  Pods
//
//  Created by cysu on 2020/11/18.
//

FOUNDATION_EXPORT NSString *const XmCancel;

/**
 * 成功回调
 */
typedef void (^XmResolveBlock)(id result);

/**
 * 失败回调
 */
typedef void (^XmRejectBlock)(NSString * code, NSString *message, NSError *error);
#define enumToString(value)  @#value
