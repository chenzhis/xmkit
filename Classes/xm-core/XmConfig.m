//
//  XmConfig.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "XmConfig.h"
#import "XmAppDelegate.h"

@implementation XmConfig

static NSDictionary *envConfig = nil;

+ (void)load {
    xm_register_module(self);
}

+ (instancetype)shared {
    return xm_get_module_instance([self class]);
}

- (instancetype)initWithConfig:(NSDictionary *)config {
    envConfig = config;

    if (self = [super init]) {
    }
    _webBaseUrl = [config valueForKeyPath:@"webBaseUrl"];
    _httpBaseUrl = [config valueForKeyPath:@"httpBaseUrl"];
    return self;
}

- (nullable id)valueForKeyPath:(NSString *)keyPath {
    return [envConfig valueForKeyPath:keyPath];
}

@end
