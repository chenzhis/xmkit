//
//  XMBaseViewController.m
//  TaiYuanBus
//
//  Created by cysu on 2020/11/13.
//  Copyright © 2020 Yosing. All rights reserved.
//

#import "XMBaseViewController.h"
#import "Masonry.h"
#import "NSBundle+xmkit.h"

@interface XMBaseViewController ()

@end

@implementation XMBaseViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:true animated:false];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationBar = [[NSBundle xmBundle] loadNibNamed:@"XMNavigationBar" owner:nil options:nil].firstObject;

    [self.view addSubview:self.navigationBar];
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ? : 0.0));
    [self.navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(topbarHeight);
    }];

    UIImage *image = [UIImage imageNamed:@"title_bar_back" inBundle:[NSBundle xmBundle] compatibleWithTraitCollection:nil];
    [self.navigationBar setLeftItemWithImage:image target:self action:@selector(backButtonAction:)];

    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setRightItemWithTitle:(NSString *)title color:(nonnull UIColor *)color font:(nonnull UIFont *)font {
    [self.navigationBar setRightItemWithTitle:title color:color font:font target:self action:@selector(rightButtonAction:)];
}

- (void)rightButtonAction:(id)sender
{
}

- (void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    self.navigationBar.title = title;
}

- (void)setNavigationBarHidden:(BOOL)navigationBarHidden
{
    _navigationBarHidden = navigationBarHidden;
    if (navigationBarHidden) {
        [self.navigationBar removeFromSuperview];
    }
}

@end
