//
//  NSBundle+xmkit.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "NSBundle+xmkit.h"
#import "XMNavigationBar.h"

@implementation NSBundle (xmkit)

+ (NSBundle *)xmBundle
{
    NSBundle *bundle = [NSBundle bundleForClass:[XMNavigationBar class]];
    NSURL *burl = [bundle URLForResource:@"XMKit" withExtension:@"bundle"];
    NSBundle *fbundle = [NSBundle bundleWithURL:burl];
    return fbundle;
}

@end
