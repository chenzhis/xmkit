//
//  XMNavigationBar.h
//  TaiYuanBus
//
//  Created by cysu on 2020/11/13.
//  Copyright © 2020 Yosing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XMNavigationBar : UIView
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *title;

- (void)setLeftItemWithImage:(UIImage *)image target:(id)target action:(SEL)action;
- (void)setRightItemWithImage:(UIImage *)image target:(id)target action:(SEL)action;
- (void)setRightItemWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font  target:(id)target action:(SEL)action;


@end

NS_ASSUME_NONNULL_END
