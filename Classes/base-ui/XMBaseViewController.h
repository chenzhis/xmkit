//
//  XMBaseViewController.h
//  TaiYuanBus
//
//  Created by cysu on 2020/11/13.
//  Copyright © 2020 Yosing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMNavigationBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface XMBaseViewController : UIViewController

@property (nonatomic, strong) XMNavigationBar *navigationBar;

@property (nonatomic, assign) BOOL navigationBarHidden;

- (void)setRightItemWithTitle:(NSString *)title color:(nonnull UIColor *)color font:(nonnull UIFont *)font;

- (void)rightButtonAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
