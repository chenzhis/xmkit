//
//  NSBundle+xmkit.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSBundle (xmkit)

+ (NSBundle *)xmBundle;

@end

NS_ASSUME_NONNULL_END
