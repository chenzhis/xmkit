//
//  XMNavigationBar.m
//  TaiYuanBus
//
//  Created by cysu on 2020/11/13.
//  Copyright © 2020 Yosing. All rights reserved.
//

#import "XMNavigationBar.h"
#import "NSBundle+xmkit.h"

@implementation XMNavigationBar

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIImage *image = [UIImage imageNamed:@"title_bar_back" inBundle:[NSBundle xmBundle] compatibleWithTraitCollection:nil];
    [self.leftButton setImage:image forState:UIControlStateNormal];
}

- (void)setLeftItemWithImage:(UIImage *)image target:(id)target action:(SEL)action
{
    [self.leftButton setImage:image forState:UIControlStateNormal];
    [self.leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)setRightItemWithImage:(UIImage *)image target:(id)target action:(SEL)action
{
    [self.rightButton setImage:image forState:UIControlStateNormal];
    [self.rightButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)setRightItemWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font  target:(id)target action:(SEL)action
{
    [self.rightButton setTitle:title forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = font;
    [self.rightButton setTitleColor:color forState:UIControlStateNormal];
    [self.rightButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLabel.text = title;
}

@end
