//
//  NSURL+Webp.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "NSURL+Webp.h"
#import "XMDefine.h"

@implementation NSURL (Webp)

+ (NSURL *)urlWithWebp:(NSString *)url
{
    if (IsStringEmpty(url)) {
        return nil;
    }

    if ([url containsString:@"x-oss-process"]) {
        return [NSURL URLWithString:url];
    }

    if ([url hasPrefix:@"http://" ] || [url hasPrefix:@"https://" ]) {
        if ([url containsString:@"?"]) {
            url = [[url stringByAppendingString:[NSString stringWithFormat:@"&x-oss-process=image"]] stringByAppendingString:@"/format,webp"];
        } else {
            url = [[url stringByAppendingString:[NSString stringWithFormat:@"?x-oss-process=image"]] stringByAppendingString:@"/format,webp"];
        }
    }

    return [NSURL URLWithString:url];
}

+ (NSURL *)roundUrlWithWebp:(NSString *)url
{
    if (IsStringEmpty(url)) {
        return nil;
    }

    if ([url containsString:@"x-oss-process"]) {
        return [NSURL URLWithString:url];
    }

    if ([url hasPrefix:@"http://" ] || [url hasPrefix:@"https://" ]) {
        if ([url containsString:@"?"]) {
            url = [[url stringByAppendingString:[NSString stringWithFormat:@"&x-oss-process=image"]] stringByAppendingString:@"/circle,r_1000/format,webp"];
        } else {
            url = [[url stringByAppendingString:[NSString stringWithFormat:@"?x-oss-process=image"]] stringByAppendingString:@"/circle,r_1000/format,webp"];
        }
    }

    return [NSURL URLWithString:url];
}

@end
