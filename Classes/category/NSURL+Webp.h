//
//  NSURL+Webp.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSURL (Webp)

+ (NSURL *)urlWithWebp:(NSString *)url;

+ (NSURL *)roundUrlWithWebp:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
