//
//  NSDictionary+Value.m
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import "NSDictionary+Value.h"
#import "XMDefine.h"

@implementation NSDictionary (Value)

- (NSString *)valueForString:(NSString *)key {
    if ([self objectForKey:key] == nil || [self objectForKey:key] == [NSNull null]) {
        return @"";
    }
    if ([[self objectForKey:key] isKindOfClass:[NSString class]]) {
        return [self objectForKey:key];
    }
    return [[self objectForKey:key] stringValue];
}

- (NSInteger)valueForInteger:(NSString *)key {
    if ([self objectForKey:key] == nil || [self objectForKey:key] == [NSNull null]) {
        return 0;
    }
    return [[self objectForKey:key] integerValue];
}

- (double)valueForDouble:(NSString *)key {
    if ([self objectForKey:key] == nil || [self objectForKey:key] == [NSNull null]) {
        return 0.0;
    }
    return [[self objectForKey:key] doubleValue];
}

- (NSArray *)valueForArray:(NSString *)key {
    if ([self objectForKey:key] == nil || [self objectForKey:key] == [NSNull null]) {
        return nil;
    }
    if ([[self objectForKey:key] isKindOfClass:[NSArray class]]) {
        return [self objectForKey:key];
    }
    return nil;
}

@end
