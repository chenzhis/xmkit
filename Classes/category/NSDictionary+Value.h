//
//  NSDictionary+Value.h
//  XMKit
//
//  Created by cysu on 2020/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Value)

- (NSString *)valueForString:(NSString *)key;
- (NSInteger)valueForInteger:(NSString *)key;
- (double)valueForDouble:(NSString *)key;
- (NSArray *)valueForArray:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
